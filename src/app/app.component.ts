import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ErrorMessageBindingStrategy, ReactiveFormConfig } from '@rxweb/reactive-form-validators';
import { IS_LOGIN, LOGIN_USER, TOKEN } from './domain/constant/constants';
import { ConfirmDialogService } from './domain/service/dialog/confirm-dialog-service';
import { NotificationService } from './domain/service/notification/notificaiton-service';
import { StorageService } from './domain/service/storage-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  title = 'my-app';
  isLogin = false;
  constructor(private router: Router,
    private confirmationDialogService: ConfirmDialogService,
    private storageService:StorageService,
    private notificationService:NotificationService
    ) {

  }
  ngOnInit(): void {
    
    this.isLogin = localStorage.getItem(IS_LOGIN) ? true : false;
    if (this.isLogin) {
      document.body.className = "skin-purple-light sidebar-mini";
    } else {
      document.body.className = "hold-transition login-page";
      this.router.navigate(['/login'])
    }
    ReactiveFormConfig.set({
      "reactiveForm": {
        "errorMessageBindingStrategy": ErrorMessageBindingStrategy.OnSubmit
      },
      "internationalization": {
        "dateFormat": "dmy",
        "seperator": "/"
      },
      "validationMessage": {
        "alpha": "Only alphabelts are allowed.",
        "alphaNumeric": "Only alphabet and numbers are allowed.",
        "compare": "inputs are not matched.",
        "contains": "value is not contains in the input",
        "creditcard": "creditcard number is not correct",
        "digit": "Only digit are allowed",
        "email": "email is not valid",
        "greaterThanEqualTo": "please enter greater than or equal to the joining age",
        "greaterThan": "please enter greater than to the joining age",
        "hexColor": "please enter hex code",
        "json": "please enter valid json",
        "lessThanEqualTo": "please enter less than or equal to the current experience",
        "lessThan": "please enter less than or equal to the current experience",
        "lowerCase": "Only lowercase is allowed",
        "maxLength": "maximum length is exceeded",
        "maxNumber": "enter value less than equal to 3",
        "minNumber": "enter value greater than equal to 1",
        "password": "please enter valid password",
        "pattern": "please enter valid zipcode",
        "range": "please enter age between 18 to 60",
        "required": "this field is required",
        "time": "Only time format is allowed",
        "upperCase": "Only uppercase is allowed",
        "url": "Only url format is allowed",
        "zipCode": "enter valid zip code",
        "minLength": "minimum length is 10 digit",
        "numeric": "Only numbers are allowed."
      }
    });
  }

  logout(){
    // this.subscription.push(this.http.post<APIResponseViewModel>('UserAuthorization/logout', null).subscribe((response) => {
    //   if (response && response.response) {
    //     location.href = "/home";
    //     this.storageService.remove(TOKEN);
    //     this.storageService.remove(IS_LOGIN);
    //     this.storageService.remove(LOGIN_USER);
    //     this.isLogin = false;
    //     this.notificationService.success("Logout successfully");
    //   }
    // }))
    location.href = "/#/home";
    location.reload();
    this.storageService.remove(TOKEN);
    this.storageService.remove(IS_LOGIN);
    this.storageService.remove(LOGIN_USER);
    this.notificationService.success("Logout successfully");
  }
}
