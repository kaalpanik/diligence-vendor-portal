import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';

import { LoginComponent } from './components/login/login.component';
import { SharedModule } from './shared/shared.module';
import { NotificationService } from './domain/service/notification/notificaiton-service';
import { BraodcastService } from './domain/service/broadcast-service';
import { ConfirmDialogService } from './domain/service/dialog/confirm-dialog-service';
import { ConfirmDialogComponent } from './domain/service/dialog/confirm-dialog.component';
import { NgSelectModule } from '@ng-select/ng-select';

import { NgxChartsModule } from '@swimlane/ngx-charts';
import { jqxChartModule } from 'jqwidgets-ng/jqxchart';
import { CommonModule } from '@angular/common';
import { jqxGridModule } from 'jqwidgets-ng/jqxgrid';
import { jqxButtonModule } from 'jqwidgets-ng/jqxbuttons';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AquaProSignalRService } from './domain/real-time-service';
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';
@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    
    LoginComponent,
    ConfirmDialogComponent,
    
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    SharedModule,
    NgSelectModule,
    jqxChartModule,
    CommonModule,
    NgxChartsModule,
    jqxGridModule,
    NgxDaterangepickerMd.forRoot(),
    jqxButtonModule
  ],
  exports:[SharedModule],
  providers:[NotificationService,AquaProSignalRService,BraodcastService,ConfirmDialogService ],
  bootstrap: [AppComponent],
  entryComponents:[ConfirmDialogComponent]
})
export class AppModule { }
