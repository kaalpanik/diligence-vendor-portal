import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { json, RxFormBuilder } from '@rxweb/reactive-form-validators';
import * as moment from 'moment';
import { AquaProSignalRService } from 'src/app/domain/real-time-service';
import { BraodcastService } from 'src/app/domain/service/broadcast-service';
import { ConfirmDialogService } from 'src/app/domain/service/dialog/confirm-dialog-service';
import { NotificationService } from 'src/app/domain/service/notification/notificaiton-service';
import { StorageService } from 'src/app/domain/service/storage-service';
import { APIResponseViewModel } from 'src/app/model/api-response-view-model';
import { BaseModel } from 'src/app/shared/base.component';
import * as XLSX from 'xlsx';
declare var $: any;
declare var Highcharts: any;
export var single = [
  {
    "name": "Germany",
    "value": 8940000
  },
  {
    "name": "USA",
    "value": 5000000
  },
  {
    "name": "France",
    "value": 7200000
  }
];

export var multi = [
  {
    "name": "Data",
    "series": [
      {
        "name": "2010",
        "value": 7300000
      },
      {
        "name": "2011",
        "value": 8940000
      }
    ]
  }
];


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent extends BaseModel implements OnInit {
  logDate = new FormControl();  
  historyDate = new FormControl();
  isDataCome = false;
  rPhaseUpdate: any;
  temperatureNew: any;
  humidityNew: any;
  yPhaseUpdate: any;
  bPhaseUpdate: any;
  rPhase = 0;
  yPhase = 0;
  bPhase = 0;
  newTemp = 0;
  newHum = 0;
  data: any;
  multiTemp: any[] = [];
  multiHum: any[] = [];
  gradient: boolean = true;
  showLegend: boolean = true;
  showLabels: boolean = true;
  isDoughnut: boolean = false;
  legendPosition: string = 'below';
  single: any[];
  singleNew: any[];
  multi: any[];
  packet_id: any = 0;
  gateway_id: any = 0;
  device_id: any = 0;
  epoach: any = 10;
  fm_version: any = 10;
  data_type: any = 10;
  bat_level: any = 10;
  device_temp: any = 10;
  fault: any = 10;
  epoch: Date;
  view: any[] = [800, 600];
  packet: any;
  // options
  showXAxis = true;
  showYAxis = true;
  showXAxisLabel = true;
  xAxisLabel = 'Time';
  showYAxisLabel = true;
  showYAxisLabel1 = true;
  yAxisLabel = 'Celcius';
  yAxisLabel1 = 'Humidity';
  date: Date = new Date();
  updateInterval: any;
  counter = 10;
  autoScale = true;
  excelData: any[] = [];
  todayDate: any;
  colorScheme = {
    domain: ['#5AA454', '#E44D25', '#CFC0BB', '#7aa3e5', '#a8385d', '#aae3f5']
  };
  value: any = 0;
  previousValue: number = 70;
  units: string = 'Celsius';

  constructor(private http: HttpClient,
    private rxFormBuilder: RxFormBuilder,
    private storageService: StorageService,
    private notificationService: NotificationService,
    private braodcastService: BraodcastService,
    private router: Router,
    private confirmationDialogService: ConfirmDialogService,
    private aquaProSignalRService: AquaProSignalRService
  ) {
    super();

    Object.assign(this, { single })

    this.multi = [{
      "name": "Device Status",
      "series": this.initData()
    }];

    this.multiHum = [{
      "name": "Data",
      "series": this.initData()
    }];


    this.multiTemp = [{
      "name": "Data",
      "series": this.initData()
    }];
  }

  setCharAt(str, index, chr) {
    if (index > str.length - 1) return str;
    return str.substring(0, index) + chr
  }
  ngOnInit(): void {
    this.logDate.patchValue(moment(new Date()));
    this.historyDate.patchValue(moment(new Date()));
    this.aquaProSignalRService.startConnection();
    this.aquaProSignalRService.addTransferChartDataListener();
    this.aquaProSignalRService.mxChipData.subscribe(data => {
      if (data && data) {
        this.isDataCome = true;
        let parseData = JSON.parse(data);

        this.packet = parseData;
        this.date = new Date();
        if (this.packet && this.packet.data && this.packet.data["data7"]) {
          this.newTemp = this.packet.data["data7"]
        }
        if (this.packet && this.packet.data && this.packet.data["data8"]) {
          this.newHum = this.packet.data["data8"]
        }
        this.addData();
      }
    })

    this.bindData();
    this.loadGauge();
    this.loadLatestTemp();
  }


  initData() {
    let array = new Array<any>();
    for (let i = 0; i < 10; i++) {
      array.push({
        "name": this.getTime(),//new Date(),
        "value": 0
      });
    }
    return array;
  }
  randomDate(start, end) {
    return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
  }
  addZero(i) {
    if (i < 10) {
      i = "0" + i;
    }
    return i;
  }
  getTime() {
    var d = new Date();
    var h = this.addZero(d.getHours());
    var m = this.addZero(d.getMinutes());
    var s = this.addZero(d.getSeconds());
    return h + ":" + m + ":" + s;
  }
  addData() {
    this.counter++;
    this.multi[0].series.shift();
    this.multiHum[0].series.shift();
    this.multiTemp[0].series.shift();

    const data =
    {
      "name": this.getTime(),// this.counter.toString(),
      "value": this.packet && this.packet.data && this.packet.data.data9 == 0 ? 0 : 1
    }

    const dataTemp =
    {
      "name": this.getTime(),
      "value": this.newTemp
    }
    const datahum =
    {
      "name": this.getTime(),
      "value": this.newHum
    }
    this.value = this.getRandomArbitrary(0, 100);
    //this.loadLatestTemp();
    if (this.packet && this.packet.data) {
      this.single = [
        {
          name: 'Device battery',
          value: this.packet['bat_level'] + '%'
        },
        {
          name: 'Device temperature',
          value: this.packet['device_temp'] + ' ' + 'C'
        },
        {
          name: 'Device faults',
          value: this.packet['fault']
        }
      ]
      this.singleNew = [
        {
          name: 'Device On Time',
          value: this.packet.data['data4']
        },
        {
          name: 'Device Off Time ',
          value: this.packet.data['data5']
        },
        {
          name: 'Fabric Length',
          value: this.packet.data['data6']
        },

      ]
      debugger
      this.rPhase = this.packet.data['data1'];
      this.yPhase = this.packet.data['data2'];
      this.bPhase = this.packet.data['data3'];
      var rPhase = this.rPhaseUpdate.series[0].points[0];
      rPhase.update(Number(this.rPhase));
      var yPhase = this.yPhaseUpdate.series[0].points[0];
      yPhase.update(Number(this.yPhase));
      var bPhase = this.bPhaseUpdate.series[0].points[0];
      bPhase.update(Number(this.bPhase));

      var temperatureNew = this.temperatureNew.series[0].points[0];
      temperatureNew.update(Number(this.newTemp));

      var humidityNew = this.humidityNew.series[0].points[0];
      humidityNew.update(Number(this.newHum));
      // this.loadGauge();
    }
    debugger
    this.multi[0].series.push(data);
    this.multi = [...this.multi];

    this.multiHum[0].series.push(datahum);
    this.multiHum = [...this.multiHum];

    this.multiTemp[0].series.push(dataTemp);
    this.multiTemp = [...this.multiTemp];
  }

  getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
  }

  bindData() {
    this.single = [
      {
        name: this.value,
        value: 'Device battery level'
      },
      {
        name: this.value,
        value: 'Device temperature level'
      },
      {
        name: this.value,
        value: 'Device faults'
      }

    ]
    this.singleNew = [
      {
        name: 'Device On Time',// 'RY Voltage',
        value: '0'//this.value.toString()
      },
      {
        name: 'Device Off Time', //'YB Voltage ',
        value: '0'//this.value.toString()
      },
      {
        name: 'Fabric Length',
        value: this.value
      },

    ]

    this.showComponent = true;

  }


  cardColor: string = '#232837';

  loadChart() {

  }

  onSelect(event) {
    console.log(event);
  }

  chart: any;



  loadGauge() {


    this.rPhaseUpdate = Highcharts.chart('r-phase', {

      chart: {
        type: 'gauge',
        plotBackgroundColor: null,
        plotBackgroundImage: null,
        plotBorderWidth: 0,
        plotShadow: false
      },

      title: {
        text: 'R Phase Current'
      },

      pane: {
        startAngle: -150,
        endAngle: 150,
        background: [{
          backgroundColor: {
            linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
            stops: [
              [0, '#FFF'],
              [1, '#333']
            ]
          },
          borderWidth: 0,
          outerRadius: '109%'
        }, {
          backgroundColor: {
            linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
            stops: [
              [0, '#333'],
              [1, '#FFF']
            ]
          },
          borderWidth: 1,
          outerRadius: '107%'
        }, {
          // default background
        }, {
          backgroundColor: '#DDD',
          borderWidth: 0,
          outerRadius: '105%',
          innerRadius: '103%'
        }]
      },

      // the value axis
      yAxis: {
        min: 0,
        max: 100,

        minorTickInterval: 'auto',
        minorTickWidth: 1,
        minorTickLength: 10,
        minorTickPosition: 'inside',
        minorTickColor: '#666',

        tickPixelInterval: 30,
        tickWidth: 2,
        tickPosition: 'inside',
        tickLength: 10,
        tickColor: '#666',
        labels: {
          step: 2,
          rotation: 'auto'
        },
        title: {
          text: 'R Phase Current'
        },
        plotBands: [{
          from: 0,
          to: 120,
          color: '#55BF3B' // green
        }, {
          from: 120,
          to: 160,
          color: '#DDDF0D' // yellow
        }, {
          from: 160,
          to: 200,
          color: '#DF5353' // red
        }]
      },

      series: [{
        name: 'Current',
        data: [Number(this.rPhase)],
        tooltip: {
          valueSuffix: 'AMP'
        }
      }]

    })

    this.yPhaseUpdate = Highcharts.chart('y-phase', {

      chart: {
        type: 'gauge',
        plotBackgroundColor: null,
        plotBackgroundImage: null,
        plotBorderWidth: 0,
        plotShadow: false
      },

      title: {
        text: 'Y Phase Current'
      },

      pane: {
        startAngle: -150,
        endAngle: 150,
        background: [{
          backgroundColor: {
            linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
            stops: [
              [0, '#FFF'],
              [1, '#333']
            ]
          },
          borderWidth: 0,
          outerRadius: '109%'
        }, {
          backgroundColor: {
            linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
            stops: [
              [0, '#333'],
              [1, '#FFF']
            ]
          },
          borderWidth: 1,
          outerRadius: '107%'
        }, {
          // default background
        }, {
          backgroundColor: '#DDD',
          borderWidth: 0,
          outerRadius: '105%',
          innerRadius: '103%'
        }]
      },

      // the value axis
      yAxis: {
        min: 0,
        max: 100,

        minorTickInterval: 'auto',
        minorTickWidth: 1,
        minorTickLength: 10,
        minorTickPosition: 'inside',
        minorTickColor: '#666',

        tickPixelInterval: 30,
        tickWidth: 2,
        tickPosition: 'inside',
        tickLength: 10,
        tickColor: '#666',
        labels: {
          step: 2,
          rotation: 'auto'
        },
        title: {
          text: 'Y Phase Current'
        },
        plotBands: [{
          from: 0,
          to: 120,
          color: '#55BF3B' // green
        }, {
          from: 120,
          to: 160,
          color: '#DDDF0D' // yellow
        }, {
          from: 160,
          to: 200,
          color: '#DF5353' // red
        }]
      },

      series: [{
        name: 'Current',
        data: [Number(this.yPhase)],
        tooltip: {
          valueSuffix: ' AMP'
        }
      }]

    })

    this.bPhaseUpdate = Highcharts.chart('b-phase', {

      chart: {
        type: 'gauge',
        plotBackgroundColor: null,
        plotBackgroundImage: null,
        plotBorderWidth: 0,
        plotShadow: false
      },

      title: {
        text: 'B Phase Current'
      },

      pane: {
        startAngle: -150,
        endAngle: 150,
        background: [{
          backgroundColor: {
            linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
            stops: [
              [0, '#FFF'],
              [1, '#333']
            ]
          },
          borderWidth: 0,
          outerRadius: '109%'
        }, {
          backgroundColor: {
            linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
            stops: [
              [0, '#333'],
              [1, '#FFF']
            ]
          },
          borderWidth: 1,
          outerRadius: '107%'
        }, {
          // default background
        }, {
          backgroundColor: '#DDD',
          borderWidth: 0,
          outerRadius: '105%',
          innerRadius: '103%'
        }]
      },

      // the value axis
      yAxis: {
        min: 0,
        max: 100,

        minorTickInterval: 'auto',
        minorTickWidth: 1,
        minorTickLength: 10,
        minorTickPosition: 'inside',
        minorTickColor: '#666',

        tickPixelInterval: 30,
        tickWidth: 2,
        tickPosition: 'inside',
        tickLength: 10,
        tickColor: '#666',
        labels: {
          step: 2,
          rotation: 'auto'
        },
        title: {
          text: 'B Phase Current'
        },
        plotBands: [{
          from: 0,
          to: 120,
          color: '#55BF3B' // green
        }, {
          from: 120,
          to: 160,
          color: '#DDDF0D' // yellow
        }, {
          from: 160,
          to: 200,
          color: '#DF5353' // red
        }]
      },

      series: [{
        name: 'Current',
        data: [Number(this.bPhase)],
        tooltip: {
          valueSuffix: 'AMP'
        }
      }]

    })
  }

  loadLatestTemp() {
    var gaugeOptions = {
      chart: {
        type: 'solidgauge'
      },

      title: null,

      pane: {
        center: ['50%', '100%'],
        size: '140%',
        startAngle: -90,
        endAngle: 90,
        background: {
          backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || '#EEE',
          innerRadius: '60%',
          outerRadius: '100%',
          shape: 'arc'
        }
      },

      exporting: {
        enabled: false
      },

      tooltip: {
        enabled: false
      },

      // the value axis
      yAxis: {
        stops: [
          [0.1, '#55BF3B'], // green
          [0.5, '#DDDF0D'], // yellow
          [0.9, '#DF5353'] // red
        ],
        lineWidth: 0,
        tickWidth: 0,
        minorTickInterval: null,
        tickAmount: 2,
        title: {
          y: -70
        },
        labels: {
          y: 16
        }
      },

      plotOptions: {
        solidgauge: {
          dataLabels: {
            y: 5,
            borderWidth: 0,
            useHTML: true
          }
        }
      }
    };



    this.temperatureNew = Highcharts.chart('temperature-new', Highcharts.merge(gaugeOptions, {
      yAxis: {
        min: 1,
        max: 100,
        title: {
          text: 'Temperature °C'
        }
      },

      credits: {
        enabled: false
      },

      series: [{
        name: 'Speed',
        data: [Number(this.newTemp)],
        dataLabels: {
          format:
            '<div style="text-align:center">' +
            '<span style="font-size:25px">{y}</span><br/>' +
            '<span style="font-size:12px;opacity:0.4">Temperature °C</span>' +
            '</div>'
        },
        tooltip: {
          valueSuffix: 'Temperature °C'
        }
      }]

    }));

    this.humidityNew = Highcharts.chart('humidity-new', Highcharts.merge(gaugeOptions, {
      yAxis: {
        min: 1,
        max: 100,
        title: {
          text: 'Humidity'
        }
      },

      credits: {
        enabled: false
      },

      series: [{
        name: 'Speed',
        data: [Number(this.newHum)],
        dataLabels: {
          format:
            '<div style="text-align:center">' +
            '<span style="font-size:25px">{y}</span><br/>' +
            '<span style="font-size:12px;opacity:0.4">Humidity %Rh</span>' +
            '</div>'
        },
        tooltip: {
          valueSuffix: 'Humidity %Rh'
        }
      }]

    }));

  }




  @ViewChild('TABLE', { static: false }) TABLE: ElementRef;
  title = 'Excel';
  downloadExcel() {
    this.http.get<any>('https://aquapro-api-staging.azurewebsites.net/api/packet?userpPoolId=75&date=' + this.logDate.value.startDate.format("YYYY-MM-DD") + '&startTime=14:03&endTime=15:04&logTypeId=1').subscribe(data => {
      this.excelData = [];
      debugger
      let res = JSON.parse(data.response);
      res.forEach(element => {
        this.excelData.push(JSON.parse(element.message))
      });

      setTimeout(() => {
        const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(this.TABLE.nativeElement);
        const wb: XLSX.WorkBook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
        XLSX.writeFile(wb, 'Data.xlsx');
      }, 2000);
    })

  }

  getHistory() {
    this.http.get<any>('https://aquapro-api-staging.azurewebsites.net/api/packet?userpPoolId=75&date=' + this.historyDate.value.startDate.format("YYYY-MM-DD") + '&startTime=14:03&endTime=15:04&logTypeId=1').subscribe(data => {
      this.excelData = [];
      debugger
      let res = JSON.parse(data.response);
      res.forEach(element => {
        let device = JSON.parse(element.message);
        let test = this.unixToDate(device.epoach)
        const packetDetail =
        {
          "name": element.createdDate,// this.counter.toString(),
          "value": device && device.data && device.data.data9 == 0 ? 0 : 1
        }
        this.multi[0].series.push(packetDetail);
        this.multi = [...this.multi];
      });

    })

  }

  unixToDate(time) {
    let unix_timestamp = time
    // Create a new JavaScript Date object based on the timestamp
    // multiplied by 1000 so that the argument is in milliseconds, not seconds.
    var date = new Date(unix_timestamp * 1000);
    // Hours part from the timestamp
    var hours = date.getHours();
    // Minutes part from the timestamp
    var minutes = "0" + date.getMinutes();
    // Seconds part from the timestamp
    var seconds = "0" + date.getSeconds();

    // Will display time in 10:30:23 format
    var formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
    return formattedTime;
  }
}
