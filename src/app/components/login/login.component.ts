import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RxFormBuilder, RxFormGroup } from '@rxweb/reactive-form-validators';
import { IS_LOGIN, LOGIN_USER, TOKEN } from 'src/app/domain/constant/constants';
import { BraodcastService } from 'src/app/domain/service/broadcast-service';
import { NotificationService } from 'src/app/domain/service/notification/notificaiton-service';
import { StorageService } from 'src/app/domain/service/storage-service';
import { APIResponseViewModel } from 'src/app/model/api-response-view-model';
import { BaseModel } from 'src/app/shared/base.component';
import { LoginViewModel } from './login.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent extends BaseModel implements OnInit {

  loginFormGroup: RxFormGroup;
  constructor(private http: HttpClient,
    private rxFormBuilder: RxFormBuilder,
    private storageService: StorageService,
    private notificationService: NotificationService,
    private braodcastService: BraodcastService,
    private router: Router
  ) {
    super();

  }

  ngOnInit(): void {
    let login = new LoginViewModel();
    this.loginFormGroup = <RxFormGroup>this.rxFormBuilder.formGroup(LoginViewModel, login)
    this.showComponent = true;
  }

  
  login() {
    this.loginFormGroup.submitted = true;
    if (this.loginFormGroup.valid) {
      this.subscription.push(this.http.post<APIResponseViewModel>('UserAuthentication/login', this.loginFormGroup.value).subscribe((response) => {
        if (response && response.response) {
          this.storageService.set(IS_LOGIN, true);
          this.storageService.set(TOKEN, response.response.token);
          this.storageService.set(LOGIN_USER, response.response);
          this.braodcastService.sendData(response.response)
          this.notificationService.success("Login successfully");
          setTimeout(() => {
            location.href='/dashboard'
          }, 1000);
        }
      }))
    }
  }
}
