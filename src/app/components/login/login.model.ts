import { maxLength, minLength, prop, required } from "@rxweb/reactive-form-validators";

export class LoginViewModel {

    @prop()
    @required()
    @maxLength({ value: 10 })
    @minLength({ value: 10 })
    mobileNumber: string;

    @prop()
    @required()
    otp: string;
}