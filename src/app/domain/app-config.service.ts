import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StorageService } from './service/storage-service';
import { TOKEN } from './constant/constants';


@Injectable()
export class AppConfigService {

  public version: string;
  public apiEndpointSomeData: string;

  constructor(private http: HttpClient, private storageService: StorageService) { }

  load(): Promise<any> {

    const promise = this.http.get('UserAuthentication/AnonymousLogin')
      .toPromise()
      .then((data: any) => {
        Object.assign(this, data);
        if (!this.storageService.isLogin) {
          this.storageService.set(TOKEN, data.response);
        }
        return data;
      });

    return promise;
  }
}
