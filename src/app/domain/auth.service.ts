import { Injectable } from '@angular/core';
import decode from 'jwt-decode';
import { TOKEN } from './constant/constants';
@Injectable()
export class AuthService {
  public getToken(): string {
    const data = localStorage.getItem(TOKEN);
    if(data){
      return JSON.parse(data);
    }
    return '';
  }
  public isAuthenticated(): boolean {
    // get the token
    const token = this.getToken();
    // return a boolean reflecting 
    // whether or not the token is expired
    return true;
  }

}
