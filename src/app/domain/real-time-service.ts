import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import * as SignalR from '@aspnet/signalr';
import * as signalR from "@aspnet/signalr";  //
@Injectable({
    providedIn: 'root'
})

export class AquaProSignalRService {
    mxChipData: Subject<any> = new Subject();
    //   mxChipDataWatts: Subject<any> = new Subject();
    //   mxChipDataNew: Subject<any> = new Subject();
    //   private hubConnection: SignalR.HubConnection;
    //   private hubConnectionNew: SignalR.HubConnection;

    //   constructor(private http: HttpClient,
    //     ) {
    //   }

    //   private getSignalRConnection(): Observable<any> {
    //     return this.http.get<any>("");
    //   }

    //   init(poolId: string, userPoolId: number = 0) {
    //     this.getSignalRConnection().subscribe(con => {
    //       const options = {
    //         transport: SignalR.HttpTransportType.ServerSentEvents,
    //         accessTokenFactory: () => con.accessToken
    //       };

    //       let data = options;
    //       this.hubConnection = new SignalR.HubConnectionBuilder()
    //         .withUrl(con.url, options)
    //         .build();

    //       this.hubConnection.on(poolId, data => {
    //         this.mxChipData.next(data);
    //       });

    //       this.hubConnection.start()
    //         .catch(error => error);

    //       this.hubConnection.serverTimeoutInMilliseconds = 300000;
    //       this.hubConnection.keepAliveIntervalInMilliseconds = 300000;

    //       this.hubConnection.onclose((error) => {
    //         //this.hubConnection.start();

    //       });
    //     });
    //   }

    //   initWatts(userPoolId: string) {
    //     this.getSignalRConnection().subscribe(con => {
    //       const options = {
    //         transport: SignalR.HttpTransportType.ServerSentEvents,
    //         accessTokenFactory: () => con.accessToken
    //       };

    //       let data = options;
    //       this.hubConnection = new SignalR.HubConnectionBuilder()
    //         .withUrl(con.url, options)
    //         .build();

    //       this.hubConnection.on(userPoolId, data => {
    //         this.mxChipDataWatts.next(data);
    //       });

    //       this.hubConnection.start()
    //         .catch(error => error);

    //       this.hubConnection.serverTimeoutInMilliseconds = 300000;
    //       this.hubConnection.keepAliveIntervalInMilliseconds = 300000;

    //       this.hubConnection.onclose((error) => {
    //         //this.hubConnection.start();

    //       });
    //     });
    //   }

    //   transaction(userPoolId: string) {
    //     this.mxChipDataNew = new Subject();
    //     this.hubConnectionNew = new SignalR.HubConnectionBuilder()
    //       .withUrl("http://aquapro-api-staging.azurewebsites.net/aquaProHub")
    //       .build();

    //     this.hubConnectionNew.on("99999", data => {
    //       debugger
    //       this.mxChipDataNew.next(data);
    //     });

    //     this.hubConnectionNew.start()
    //       .catch(error => error);

    //     this.hubConnectionNew.serverTimeoutInMilliseconds = 300000;
    //     this.hubConnectionNew.keepAliveIntervalInMilliseconds = 300000;

    //     this.hubConnectionNew.onclose((error) => {


    //     });
    //   }

    //   close(){
    //     this.mxChipDataNew.unsubscribe();
    //     this.hubConnectionNew .onclose((error) => {
    //       //this.hubConnection.start();

    //     });
    //   }

    public data: any;
    private hubConnection: signalR.HubConnection
    public startConnection = () => {
        this.hubConnection = new signalR.HubConnectionBuilder()
            .withUrl('https://aquapro-api-staging.azurewebsites.net/aquaProHub')
            .build();
        this.hubConnection
            .start()
            .then(() => console.log('Connection started'))
            .catch(err => console.log('Error while starting connection: ' + err))
    }
    public addTransferChartDataListener = () => {
        this.hubConnection.on('99999', (data) => {
            this.data = data;
            this.mxChipData.next(data);
            console.log(data);
        });
    }
}

// 214
