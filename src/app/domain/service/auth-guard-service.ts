import { CanActivate, Router } from "@angular/router";
import { Injectable } from "@angular/core";
import { IS_LOGIN } from "../constant/constants";
import { StorageService } from "./storage-service";
import jwt_decode from "jwt-decode";
@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private router: Router,
  
    ) {
  }

  canActivate() {
    let isLoggedIn = localStorage.getItem(IS_LOGIN) == "true"
    if (isLoggedIn) {
      return true;
    }
    this.router.navigate(['/login']);
    return false;
  }
}

@Injectable()
export class AuthAdminGuard implements CanActivate {
  constructor(private router: Router,
    private storageService: StorageService,
    ) {
  }

  canActivate() {
    let isLoggedIn = localStorage.getItem(IS_LOGIN) == "true"
    if (this.checkAdminUser()) {
      return true;
    }
    this.router.navigate(['/home']);
    return false;
  }

  checkAdminUser():boolean {
    let parseToken = null;
    let token = this.storageService.get('token')
    if (token) {
      parseToken = jwt_decode(token);
      if (parseToken) {
        let getRoleId = parseToken['http://schemas.microsoft.com/ws/2008/06/identity/claims/role'];
        if (getRoleId == 1) {
         return true;
        }
      }
    }
    return false;
  }
}

@Injectable()
export class AuthUserGuard implements CanActivate {
  constructor(private router: Router,
    private storageService: StorageService,
    ) {
  }

  canActivate() {
    let isLoggedIn = localStorage.getItem(IS_LOGIN) == "true"
    if (this.checkUser()) {
      return true;
    }
    this.router.navigate(['/home']);
    return false;
  }

  checkUser():boolean {
    let isLoggedIn = localStorage.getItem(IS_LOGIN) == "true"
    let parseToken = null;
    let token = this.storageService.get('token')
    if (token && isLoggedIn) {
      parseToken = jwt_decode(token);
      if (parseToken) {
        let getRoleId = parseToken['http://schemas.microsoft.com/ws/2008/06/identity/claims/role'];
        if (getRoleId == 3) {
         return true;
        }else{
          return false;
        }
      }
    }
    return true;
  }
}
