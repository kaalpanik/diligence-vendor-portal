import { Observable, Subject } from "rxjs";
import { Injectable, EventEmitter } from "@angular/core";
import { filter, map } from "rxjs/operators";
export enum AppEventType {
  ClickedOnNotification = 'CLICKED_ON_NOTIFICATION',
  SocketEvent = 'SOCKET_EVENT',
  CartUpdate = 'CartUpdate'
}
export class AppEvent<T> {
  constructor(
    public type: AppEventType,
    public payload: T,
  ) {}
}
@Injectable()
export class BraodcastService {
  public sendDataSubject: Subject<any> = new Subject<any>();
  public sendDataSusciber: Observable<any>

  public sendCartSubject: Subject<any> = new Subject<any>();
  public sendCartSusciber: Observable<any>

  private eventBrocker = new Subject<AppEvent<any>>();

  
  constructor() {
    this.sendDataSusciber = this.sendDataSubject.asObservable();
    this.sendCartSusciber = this.sendCartSubject.asObservable();
  }

  sendData(data: any): void {
    this.sendDataSubject.next(data);
  }

  updateCart(data: any) {
    debugger
    this.sendCartSubject.next(data);
  } 

  on(eventType: AppEventType): Observable<AppEvent<any>> {
    return this.eventBrocker.pipe(filter(event => event.type === eventType));
  }

  dispatch<T>(event: AppEvent<T>): void {
    this.eventBrocker.next(event);
  }
  
}
