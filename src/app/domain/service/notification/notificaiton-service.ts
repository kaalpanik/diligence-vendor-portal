import { Injectable } from "@angular/core";
declare var toastr: any;
@Injectable()
export class NotificationService {
  constructor() { }


 public success(text: any) {
    toastr.success(text)
  }
  public warning(text: any) {
    toastr.error(text)
  }
  public error(text: any) {
    toastr.error(text)
  }
}
