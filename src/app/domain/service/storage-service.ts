import { Injectable } from "@angular/core";
import { TOKEN, LOGIN_USER, IS_LOGIN } from '../constant/constants';
import { ROLE_TYPES } from '../enum/enums';
import jwt_decode from "jwt-decode";
@Injectable()
export class StorageService {

  constructor() { }
  set(key: string, data: any): void {
    try {
      localStorage.setItem(key, JSON.stringify(data));
    } catch (e) {
      console.error('Error saving to localStorage', e);
    }
  }

  get(key: string) {
    try {
      const data = localStorage.getItem(key);
      if (data) {
        return JSON.parse(data);
      }
    } catch (e) {
      console.error('Error getting data from localStorage', e);
      return null;
    }
  }
  remove(key: string) {
    try {
      localStorage.removeItem(key);
    } catch (e) {
      console.error('Error getting data from localStorage', e);
      return null;
    }
  }

  get userId(): number {
    const loginUser = localStorage.getItem(LOGIN_USER);
    if (loginUser) {
      let data = JSON.parse(loginUser);
      if (data) {
        return data.userId;
      }
    }
    return 1;
  }

  get mobileNumber(): string {
    const loginUser = localStorage.getItem(LOGIN_USER);
    if (loginUser) {
      let data = JSON.parse(loginUser);
      if (data) {
        return data.mobileNumber;
      }
    }
    return '';
  }

  get roleName(): string {
    const loginUser = localStorage.getItem(LOGIN_USER);
    if (loginUser) {
      let data = JSON.parse(loginUser);
      let roleName = ROLE_TYPES.find(x => x.id == data.roleId);
      if (roleName) {
        return roleName.name;
      }
    }
    return '';
  }

  get roleId(): number {
    const loginUser = localStorage.getItem(LOGIN_USER);
    if (loginUser) {
      let data = JSON.parse(loginUser);
      if (data) {
        return data.roleId;
      }
    }
    return 0;
  }

  get isLogin(): boolean {
    let data = localStorage.getItem(IS_LOGIN);
    if (data) {
      return true;
    }
    return false;
  }
  checkAdminUser():boolean {
    let parseToken = null;
    let token = this.get('token')
    if (token) {
      parseToken = jwt_decode(token);
      if (parseToken) {
        let getRoleId = parseToken['http://schemas.microsoft.com/ws/2008/06/identity/claims/role'];
        if (getRoleId == 1) {
         return true;
        }else{
          return false;
        }
      }
    }
    return false;
  }
}
