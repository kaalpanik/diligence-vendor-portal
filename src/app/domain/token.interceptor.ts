import { Injectable } from '@angular/core';
import {
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpEvent,
  HttpErrorResponse,
  HttpResponse,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { AuthService } from './auth.service';
import { tap, catchError, map, finalize } from 'rxjs/operators';
import { NotificationService } from './service/notification/notificaiton-service';
declare var toastr: any;
@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  private cache = new Map<string, any>();
  byPassApiList: any[] = ["UserAuthentication/ForgotPassword", "UserAuthentication/OtpVerification", "UserAuthentication/login", "UserAuthentication/AnonymousLogin"]
  //baseURL: string = "http://turfapi-dev.ap-south-1.elasticbeanstalk.com/api/";
  baseURL: string = "https://billingapi.yesguruji.com/api/";
  data = this;
  constructor(public auth: AuthService,
    private notificationService: NotificationService,
  ) { }
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!(this.byPassApiList.find(x => x === request.url || !x.includes('aquapro-api-staging.azurewebsites')))) {
      request = request.clone({
        setHeaders: {
          Authorization: `${this.auth.getToken()}`
        },
        url: this.baseURL + request.url
      })
    } else {
      if(!request.url.includes('aquapro-api-staging.azurewebsites')){
        request = request.clone({
          url: this.baseURL + request.url
        })
      }else{
        request = request.clone({
          url:  request.url
        })
      }
      
    }
    let data = document.getElementById("loader");
    if (data) {
      data.classList.add("loading")
    }

    return next.handle(request).pipe(
      tap((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse && event.status === 200) {

        }
      }),
      catchError(this.handleError), finalize(() => {
        this.hideLoader();
      }),
    ) as Observable<HttpEvent<any>>;
  }

  hideLoader() {
    let data = document.getElementById("loader");
    if (data) {
      data.classList.remove("loading")
    }
  }
  handleError(error: HttpErrorResponse) {
    if (error && error.status === 400) {
      toastr.error(error.error.message)
      let data = document.getElementById("loader");
      if (data) {
        data.classList.remove("loading")
      }
    } else {
      let data = document.getElementById("loader");
      if (data) {
        data.classList.remove("loading")
      }
      return throwError(error);

    }
    console.log("lalalalalalalala");
    return throwError(error);
  }
}
