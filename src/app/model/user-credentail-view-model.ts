import { prop, propObject, propArray, required, compare, maxLength, range, email, numeric, minLength } from "@rxweb/reactive-form-validators"

export class UserSignUpModel {

  constructor() {

  }

  @prop()
  @required()
  @numeric({allowDecimal:false})
  @minLength({value:10})
  @maxLength({value:10})
  mobileNumber: string;

  @prop()
  otp: string;

  @prop()
  otpVerification: boolean = false;

  @prop()
  isForGotPassword: boolean = false;

  @prop()
  @required()
  userName: string;

  @prop()
  @required()
  dateOfBirth: string;

  @prop()
  referCode: string;

  @prop()
  @required()
  password: string;

  @prop()
  @required()
  @compare({ fieldName: 'password', message: "password and confirm password not match" })
  confirmPassword: string;

}

export class UserLoginModel {

  @prop()
  @numeric({allowDecimal:false})
  @maxLength({value:10})
  @minLength({value:10})
  mobileNumber: string;

  @prop()
  password: string;

  @prop()
  IsAdmin: boolean = true;
}

export class UserResetPasswordModel {

  @prop()
  @numeric({allowDecimal:false})
  @maxLength({value:10})
  @minLength({value:10})
  @required()
  mobileNumber: string;

  @prop()
  @required()
  password: string;

  @prop()
  @required()
  @compare({ fieldName: 'password', message: "password and confirm password not match" })
  confirmPassword: string;

  @prop()
  @required()
  otp: string;

  @prop()
  isForgotPasswordOtp:boolean;

}

export class UserProfileUpModel {

  constructor() {

  }

  @prop()
  @required()
  @numeric({allowDecimal:false})
  @minLength({value:10})
  @maxLength({value:10})
  mobileNumber: string;


  @prop()
  isChangePassword: boolean = false;

  @prop()
  @required()
  userName: string;

  @prop()
  @required()
  dateOfBirth: string;

  @prop()
  referCode: string;

  @prop()
  generatedReferCode: string;

  @prop()
  coupanCode: string;
  // @prop()
  // @required()
  // password: string;

  // @prop()
  // @required()
  // @compare({ fieldName: 'password', message: "password and confirm password not match" })
  // confirmPassword: string;

  @prop()
  userId:number;

}

export class UserPasswordModel {

  constructor() {

  }


  @prop()
  isChangePassword: boolean = true;

  @prop()
  @required()
  password: string;

  @prop()
  @required()
  @compare({ fieldName: 'password', message: "password and confirm password not match" })
  confirmPassword: string;

  @prop()
  @required()
  oldPassword:string;

  @prop()
  userId:number;

}