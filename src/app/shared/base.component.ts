import { Component, ViewChild } from "@angular/core";
import { jqxGridComponent } from "jqwidgets-ng/jqxgrid";
import * as moment from "moment";
import { LocaleConfig } from "ngx-daterangepicker-material";
import { Subscription } from "rxjs";

@Component({
  template: ''
})
export abstract class BaseModel {
  subscription: Subscription[] = [];
  showComponent = false;
  minDate = moment();
  today = moment(new Date()).format('MM/DD/YYYY');
  id: any;
  locale: LocaleConfig = {
    applyLabel: 'Appliquer',
    customRangeLabel: ' - ',
    daysOfWeek: moment.weekdaysMin(),
    monthNames: moment.monthsShort(),
    firstDay: moment.localeData().firstDayOfWeek(),
  }
  jsonRequest = {

  }
  @ViewChild('myGrid', { static: false }) myGrid: jqxGridComponent;

  getWidth(): any {
    if (document.body.offsetWidth < 850) {
      return '90%';
    }

    return '100%';
  }
  dataAdapter: any;

  excelBtnOnClick() {
    this.myGrid.exportdata('xls', 'jqxGrid');
  };
  csvBtnOnClick() {
    this.myGrid.exportdata('csv', 'jqxGrid');
  };
  htmlBtnOnClick() {
    this.myGrid.exportdata('html', 'jqxGrid');
  };

  pdfBtnOnClick() {
    this.myGrid.exportdata('pdf', 'jqxGrid');
  };

  btnOnClick() {
    let gridContent = this.myGrid.exportdata('html');
    const WindowObject = window.open('', 'PrintWindow', 'width=750,height=650,top=50,left=50,toolbars=no,scrollbars=yes,status=no,resizable=yes'
    );
    const htmlData = `<html><body>${gridContent}</body></html>`;
    if (WindowObject) {
      let document = WindowObject.document.open();
      document.write(htmlData);
      document.close();
      WindowObject.print();
      // WindowObject.document.writeln(htmlData);
      // WindowObject.document.close();
      // WindowObject.focus();
      // setTimeout(() => {
      //   WindowObject.close();
      // }, 0.5);
    };
  }

}