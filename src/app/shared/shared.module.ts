import { NgModule, PlatformRef, APP_INITIALIZER } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BraodcastService } from '../domain/service/broadcast-service';
import { AppConfigService } from '../domain/app-config.service';
import { TokenInterceptor } from '../domain/token.interceptor';
import { DataService } from '../domain/data.service';
import { AuthService } from '../domain/auth.service';
import { NotificationService } from '../domain/service/notification/notificaiton-service';
import { StorageService } from '../domain/service/storage-service';
import { AuthAdminGuard, AuthGuard, AuthUserGuard } from '../domain/service/auth-guard-service';
import { NgSelectModule } from '@ng-select/ng-select';
import { jqxGridModule } from 'jqwidgets-ng/jqxgrid';
export function appInit(appConfigService: AppConfigService) {
  return () => appConfigService.load();
}


@NgModule({
  declarations: [
  
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RxReactiveFormsModule,
    HttpClientModule,
    jqxGridModule
    
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    RxReactiveFormsModule,
    HttpClientModule,
    jqxGridModule
  ],

  providers: [AuthUserGuard,AuthAdminGuard,AppConfigService, TokenInterceptor, DataService, AuthService, NotificationService, StorageService, AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true,
     
    },
    // {
    //   provide: APP_INITIALIZER,
    //   useFactory: appInit,
    //   multi: true,
    //   deps: [AppConfigService]
    // },
  ],
})
export class SharedModule { }
